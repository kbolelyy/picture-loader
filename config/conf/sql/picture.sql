CREATE TABLE IF NOT EXISTS PICTURES (

  ID long PRIMARY KEY AUTO_INCREMENT,
  file_name VARCHAR(255),
  payload_picture BLOB,
  preview_picture BLOB,
  content_type VARCHAR(255),
);