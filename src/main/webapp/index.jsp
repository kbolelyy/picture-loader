<%--
  Created by IntelliJ IDEA.
  User: Kirill
  Date: 29.12.2018
  Time: 13:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"
      integrity="sha384-PmY9l28YgO4JwMKbTvgaS7XNZJ30MK9FAZjjzXtlqyZCqBY6X6bXIkM++IkyinN+" crossorigin="anonymous">
<head>
    <title>Upload picture</title>
</head>
<body>
<style>
    .center {
        margin: auto;
        width: 300px;
        padding: 10px;
        margin-top: 10%;
    }
</style>

<div class="center">
    <form action="${pageContext.request.contextPath}/file-uploader/load" method="POST"
          enctype="multipart/form-data">
        <div class="form-group">
            <br/>
            Please add files:
            <input class="form-control-file" type="file" name="file" multiple="multiple"/>
            <br/>
            <input class="form-control" type="submit" name="submit" value="submit"/>
            <br/>
        </div>
    </form>
    <br/>
    <form action="${pageContext.request.contextPath}/file-uploader/load" method="POST"
          enctype="multipart/form-data">
        <div class="form-group">
            <br/>
            Please paste uri for load image:
            <input class="form-control" type="text" name="fileUri"/>
            <br/>
            <input class="form-control" type="submit" name="submit" value="submit"/>
            <br/>
        </div>
    </form>
</div>




</body>
</html>