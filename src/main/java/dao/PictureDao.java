package dao;

import dto.Picture;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Dao (Data access object)
 * для сохранения наших сущностей Picture.class в БД
 * с помощью менеджера сущностей
 */
@Stateless
public class PictureDao {

    @PersistenceContext(unitName = "picture")
    EntityManager entityManager;


    public void savePicture(List<Picture> pictures) {
        for (Picture picture : pictures) {
            entityManager.persist(picture);
        }
    }

    public List<Picture> getById(Long id) {
        return entityManager.createQuery("SELECT p FROM Picture p where p.id = :id")
                .setParameter("id", id).getResultList();
    }


}
