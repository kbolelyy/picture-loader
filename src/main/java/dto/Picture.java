package dto;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

/**
 * Сущность которая сохраняется в БД SQL
 * содержит основные данные загруженных картинок
 */

@Entity
@Table(name = "PICTURES")
public class Picture {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Column(name = "file_name")
    private String fileName;

    @Column(name = "content_type")
    private String contentType;

    @Lob
    @Column(name = "payload_picture")
    private byte[] payloadPicture;

    @Lob
    @Column(name = "preview_picture")
    private byte[] previewPicture;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public byte[] getPayloadPicture() {
        return payloadPicture;
    }

    public void setPayloadPicture(byte[] payloadPicture) {
        this.payloadPicture = payloadPicture;
    }

    public byte[] getPreviewPicture() {
        return previewPicture;
    }

    public void setPreviewPicture(byte[] previewPicture) {
        this.previewPicture = previewPicture;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
}
