package dto.json;

/**
 * Класс для перевода формата JSON объекта в Java класс
 * требуемый формат {"data": "/9j/4AAQSk" , "contentType": "image/jpeg"}
 */
public class JsonPicture {

   private String data;
   private String contentType;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
}
