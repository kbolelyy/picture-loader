package rest;


import com.google.gson.Gson;
import dao.PictureDao;
import dto.Picture;
import dto.json.JsonPicture;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;
import services.PictureDataService;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Контроллер который принимает REST запросы
 * на сохранение картинок и их получение
 */

@Path("file-uploader")
public class FileUploaderController {


    @EJB
    PictureDao pictureDao;

    @EJB
    PictureDataService pictureService;


    /**
     * Метод который принимает multipart-form и json запросы для сохранения изображений
     *
     * @param request Запрос из которого будем извлекать данные
     * @return Response возвращает ответ сервера с текстом, если будет ошибка вернем клиенту код 500 и сообщением об ошибке.
     */
    @Path("/load")
    @POST
    @Consumes(value = {MediaType.MULTIPART_FORM_DATA, MediaType.APPLICATION_JSON})
    public Response uploadFile(@Context HttpServletRequest request) {

        try {
            List<Picture> pictures = new ArrayList<>();

            if (ServletFileUpload.isMultipartContent(request)) {
                List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
                for (FileItem fileItem : items) {
                    if (fileItem.getFieldName().equals("file") && fileItem.get().length != 0) {
                        pictures.add(pictureService.createPictureFileForm(fileItem));
                    }
                    if (fileItem.getFieldName().equals("fileUri") && !fileItem.getString().isEmpty()) {
                        pictures.add(pictureService.createPictureFromInternet(fileItem.getString()));
                    }
                }
            }

            if (request.getContentType().equals(MediaType.APPLICATION_JSON)) {
                String json = IOUtils.toString(request.getInputStream());
                pictures.add(pictureService.createPictureJson(new Gson().fromJson(json, JsonPicture.class)));
            }

            pictureDao.savePicture(pictures);
        } catch (Exception e){
            return Response.status(500).entity(e.getLocalizedMessage()).build();
        }
        return Response.ok("Photo load successfully. Please use this path in request " +
                "\n" + request.getContextPath() + "/file-uploader/image/{id} for load picture").build();
    }


    /**
     * Получение изображения из БД по id
     *
     * @param id картинки
     * @return Response который содержит в себе изображение
     */
    @Path("/image/{id}")
    @Produces(value = {"image/jpg", "image/bmp", "image/gif"})
    @GET
    public Response getImageById(@PathParam(value = "id") Long id) {
        List<Picture> pictures = pictureDao.getById(id);
        if (pictures.size() != 0) {
            Picture picture = pictures.get(0);

            return Response.ok(new ByteArrayInputStream(picture.getPayloadPicture()), picture.getContentType()).build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();

    }

    /**
     * Получение preview изображения из БД по id
     *
     * @param id картинки
     * @return Response который содержит в себе preview изображения
     */
    @Path("image/thumbnail/{id}")
    @Produces(value = {"image/jpg", "image/bmp", "image/gif"})
    @GET
    public Response getThumbNailById(@PathParam(value = "id") Long id) {
        List<Picture> pictures = pictureDao.getById(id);
        if (pictures.size() != 0) {
            Picture picture = pictures.get(0);

            return Response.ok(new ByteArrayInputStream(picture.getPreviewPicture()), picture.getContentType()).build();
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }

}
