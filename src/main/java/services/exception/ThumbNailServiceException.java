package services.exception;

/**
 * Исключения для сервиса создание превью изображения
 */
public class ThumbNailServiceException extends Exception {

    public ThumbNailServiceException(String message) {
        super(message);
    }

    public ThumbNailServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
