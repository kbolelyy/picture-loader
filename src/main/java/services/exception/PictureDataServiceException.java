package services.exception;

/**
 * Исключение для сервиса создания изображений
 */
public class PictureDataServiceException extends Exception {

    public PictureDataServiceException(String message) {
        super(message);
    }

    public PictureDataServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
