package services;


import org.apache.commons.io.output.ByteArrayOutputStream;
import services.exception.ThumbNailServiceException;

import javax.ejb.Stateless;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.MemoryCacheImageInputStream;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Iterator;

@Stateless
public class ThumbNailService {

    public byte[] createThumbNail(byte[] bytes, String contentType) throws ThumbNailServiceException {
        Iterator<ImageReader> readers = ImageIO.getImageReadersByMIMEType(contentType);
        BufferedImage bufferedImage = new BufferedImage(100, 100, BufferedImage.TYPE_INT_RGB);
        ImageInputStream iis = new MemoryCacheImageInputStream(new ByteArrayInputStream(bytes));
        if (readers.hasNext()) {
            ImageReader next = readers.next();
            next.setInput(iis);
            ByteArrayOutputStream bos = null;
            try {
                BufferedImage read = next.read(0);
                bufferedImage.createGraphics().drawImage(read.getScaledInstance(100, 100, Image.SCALE_SMOOTH), 0, 0, null);
                bos = new ByteArrayOutputStream();
                ImageIO.write(bufferedImage, "jpg", bos);
            } catch (IOException e) {
              throw new ThumbNailServiceException(MessageFormat.format("can`t create preview image, because : {0}", e.getLocalizedMessage()));
            }
            return bos.toByteArray();
        } else {
            throw new ThumbNailServiceException("can`t create preview image, because format not supported.");
        }
    }

}
