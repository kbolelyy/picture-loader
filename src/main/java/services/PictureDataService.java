package services;


import dto.Picture;
import dto.json.JsonPicture;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import services.exception.PictureDataServiceException;
import services.exception.ThumbNailServiceException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.text.MessageFormat;
import java.util.Base64;

/**
 * Сервис создания картинок
 */

@Stateless
public class PictureDataService {

    @EJB
    private ThumbNailService thumbNailService;


    /**
     * Метод для сохранения изображения полученного из FileItem
     *
     * @param fileItem Объект который получаем из отправленной формы клиента
     * @return сущность Picture
     * @throws ThumbNailServiceException исключение при создании превью изображения
     */
    public Picture createPictureFileForm(FileItem fileItem) throws ThumbNailServiceException, PictureDataServiceException {
        Picture picture = new Picture();
        String contentType = fileItem.getContentType();
        byte[] payloadPicture;
        try {
            payloadPicture = IOUtils.toByteArray(fileItem.getInputStream());
        } catch (IOException e) {
            throw new PictureDataServiceException(MessageFormat.
                    format("can`t read stream from file item, because: {0}", e.getLocalizedMessage()), e);
        }

        picture.setFileName(FilenameUtils.getName(fileItem.getName()));
        picture.setContentType(contentType);
        picture.setPayloadPicture(payloadPicture);
        picture.setPreviewPicture(thumbNailService.createThumbNail(payloadPicture, contentType));
        return picture;
    }

    /**
     * Метод для получения и сохранения изображения по указанному URI
     *
     * @param uri URI путь для изображения из интернета или других источников
     * @return сущность Picture
     * @throws IOException               может быть выброшено исключение если возникла проблема с чтением потоков
     * @throws ThumbNailServiceException исключение при создании превью изображения
     */
    public Picture createPictureFromInternet(String uri) throws ThumbNailServiceException, PictureDataServiceException {
        URLConnection connection = null;
        try {
            connection = new URL(uri).openConnection();
        } catch (IOException e) {
            throw new PictureDataServiceException(MessageFormat.
                    format("can`t open connection by uri {0}, because: {1}", uri, e.getLocalizedMessage()), e);
        }
        connection.setDoOutput(true);
        connection.setRequestProperty("Content-Type",
                "multipart/form-data; boundary=" + Long.toHexString(System.currentTimeMillis()));
        byte[] payloadPicture = new byte[0];
        try {
            payloadPicture = IOUtils.toByteArray(connection.getInputStream());
        } catch (IOException e) {
            throw new PictureDataServiceException(MessageFormat.
                    format("can`t get bytes from connection by uri {0}, because: {1}", uri, e.getLocalizedMessage(), e));
        }
        String contentType = connection.getContentType();
        Picture picture = new Picture();
        picture.setContentType(contentType);
        picture.setPayloadPicture(payloadPicture);
        picture.setPreviewPicture(thumbNailService.createThumbNail(payloadPicture, contentType));
        return picture;
    }


    /**
     * Метод для сохранения изображения полученного из переданного JSON объекта
     *
     * @param jsonPicture переданный нам объект JSON требуемый формат {"data": "/9j/4AAQSk" , "contentType": "image/jpeg"}
     * @return сущность Picture
     * @throws PictureDataServiceException исключение выбрасываемое по причине отсутствия данных JSON объекта
     * @throws IOException                 может быть выброшено исключение если возникла проблема с чтением потоков
     * @throws ThumbNailServiceException   исключение при создании превью изображения
     */
    public Picture createPictureJson(JsonPicture jsonPicture) throws PictureDataServiceException, ThumbNailServiceException {
        if (jsonPicture == null) {
            throw new PictureDataServiceException("json object is null, can't convert to image");
        }
        String jsonData = jsonPicture.getData();
        if (jsonData.isEmpty()) {
            throw new PictureDataServiceException("json data is empty, can`t convert to image, try again");
        }
        byte[] payloadPicture = Base64.getDecoder().decode(jsonData.getBytes());
        Picture picture = new Picture();
        picture.setContentType(jsonPicture.getContentType());
        picture.setPayloadPicture(payloadPicture);
        picture.setPreviewPicture(thumbNailService.createThumbNail(payloadPicture, jsonPicture.getContentType()));
        return picture;
    }

}
