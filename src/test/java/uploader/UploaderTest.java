package uploader;


import com.google.gson.Gson;
import dao.PictureDao;
import dto.Picture;
import dto.json.JsonPicture;
import org.apache.commons.fileupload.FileItem;
import org.apache.openejb.jee.EjbJar;
import org.apache.openejb.jee.StatelessBean;
import org.apache.openejb.jee.jpa.unit.PersistenceUnit;
import org.apache.openejb.junit.ApplicationComposer;
import org.apache.openejb.testing.Configuration;
import org.apache.openejb.testing.Module;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import services.PictureDataService;
import services.ThumbNailService;
import services.exception.PictureDataServiceException;
import services.exception.ThumbNailServiceException;

import javax.ejb.EJB;
import javax.naming.Context;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Properties;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


/* Класс для тестирования всех методов сохранения картинок */

@RunWith(ApplicationComposer.class)
public class UploaderTest {

    @EJB
    PictureDataService pictureService;


    @Module
    public PersistenceUnit persistence() {
        PersistenceUnit unit = new PersistenceUnit("picture");
        unit.setJtaDataSource("picture");
        unit.getClazz().add(Picture.class.getName());
        unit.setProperty("openjpa.jdbc.SynchronizeMappings", "buildSchema(ForeignKeys=true)");
        return unit;
    }

    @Configuration
    public Properties config() {
        Properties properties = new Properties();
        properties.put(Context.INITIAL_CONTEXT_FACTORY, "org.apache.openejb.core.LocalInitialContextFactory");
        properties.put("picture", "new://Resource?type=DataSource");
        properties.put("picture.JdbcDriver", "org.h2.Driver");
        properties.put("picture.JdbcUrl", "jdbc:h2:mem:picture;create=true;");
        properties.put("picture.JtaManaged", "true");
        return properties;
    }

    @Module
    public EjbJar beans() {
        EjbJar ejbJar = new EjbJar();
        ejbJar.addEnterpriseBean(new StatelessBean(ThumbNailService.class));
        ejbJar.addEnterpriseBean(new StatelessBean(PictureDataService.class));
        ejbJar.addEnterpriseBean(new StatelessBean(PictureDao.class));
        return ejbJar;
    }


    /* Тест на сохранение картинки и её preview из отправленной пользователем формы */
    @Test
    public void createPictureToFormFile() throws IOException, ThumbNailServiceException, PictureDataServiceException {

        File filePreview = new File("src/test/java/resources/jsonPreview.jpg");
        byte[] previewExpected = Files.readAllBytes(filePreview.toPath());

        File filePicture = new File("src/test/java/resources/picture.jpg");
        InputStream inputStream = new FileInputStream(filePicture);

        FileItem fileItemMock = mock(FileItem.class);
        when(fileItemMock.getInputStream()).thenReturn(inputStream);
        when(fileItemMock.getContentType()).thenReturn("image/jpeg");
        when(fileItemMock.getName()).thenReturn("somePictureName");

        Picture pictureFileForm = pictureService.createPictureFileForm(fileItemMock);
        byte[] previewActual = pictureFileForm.getPreviewPicture();
        Assert.assertArrayEquals("preview image from json data and local store different!", previewExpected, previewActual);
    }

    /* Тест на сохранение картинки и её preview по указанному url*/
    @Test
    public void createPictureFromUri() throws IOException, ThumbNailServiceException, PictureDataServiceException {
        String uri = "https://jpeg.org/images/jpeg2000-home.jpg";
        File file = new File("src/test/java/resources/fromInternetPreview.jpg");
        byte[] previewExpected = Files.readAllBytes(file.toPath());
        Picture pictureFromInternet = pictureService.createPictureFromInternet(uri);
        byte[] previewActual = pictureFromInternet.getPreviewPicture();
        Assert.assertArrayEquals("preview image from internet and local store different!", previewExpected, previewActual);
    }

    /* Тест на сохранение картинки и её preview при передаче данных в виде JSON BASE64 */
    @Test
    public void createPictureFromJsonData() throws IOException, ThumbNailServiceException, PictureDataServiceException {
        File file = new File("src/test/java/resources/jsonPreview.jpg");
        byte[] previewExpected = Files.readAllBytes(file.toPath());
        FileReader fileReader = new FileReader("src/test/java/resources/encode-image.json");
        JsonPicture jsonPicture = new Gson().fromJson(fileReader, JsonPicture.class);
        Picture pictureJson = pictureService.createPictureJson(jsonPicture);
        byte[] previewActual = pictureJson.getPreviewPicture();
        Assert.assertArrayEquals("preview image from json data and local store different!", previewExpected, previewActual);
    }

}
